# ----------------------------------
# General Instruction Set assembler
# JISA v1.1
# Copyright (c) 2015, John Allen
# ----------------------------------
#
# Licenced under GPL v3.0
# See LICENCE for more licencing details
#
__author__ = 'John'
import yaml
import sys
import os.path
import argparse
import string


def main():
    parser = argparse.ArgumentParser(description='Assemble your IS')
    parser.add_argument('i', help='input file')
    parser.add_argument('o', help='output file')
    parser.add_argument('--otype', help='<hex|bin> hexidecimal or binary output (Default binary)')
    parser.add_argument('-s', help='Instruction set .yaml location', default='IS.yaml')
    argv = parser.parse_args()
    imp_name = ['form', 'inst', 'args']
    i = 0
    d = {}
    for j in yaml.load_all(open(argv.s).read()):
        d[imp_name[i]] = j
        i += 1
    form = d['form']
    inst = d['inst']
    args = d['args']
    del d
    try:
        code = parse(form, inst, args, argv.i)
    except FileNotFoundError:
        print("Source file not found.")
        return
    if code[1] == 0:
        for tag in code[0]:
            code[0][code[0].index(tag)] = err_handle(tag[0], tag[1])
        rend = '\n'.join(code[0])
        print(rend)
        sys.exit(1)
    else:
        if argv.otype == 'hex':
            rend = hex_r(code[0])
        else:
            rend = bin_r(code[0])                                        # save output file
        save(argv.o, rend)


def save(resp, data):       # saves file
    file = open(resp, "w")
    file.write(data)
    file.close()


def hex_r(code):
    final = []
    for i in code:  # converts to hex
        final.append(bth(i))
    finalp = "\n".join(final)  # compiles results to string
    if os.path.isfile('LICENCE.txt'):
        return finalp
    else:
        return 'Nope'


def bin_r(code):
    final = []
    for i in code:  # converts to hex
        final.append(i)
    finalp = "\n".join(final)  # compiles results to string
    if os.path.isfile('LICENCE.txt'):
        return finalp
    else:
        return 'Nope'


def bth(binar):
    hex_v = (hex(int(binar, 2))[2:])[-4:].zfill(4)  # convertion to hex
    return hex_v


def lex(raw):
    tok = raw.split("\n")  # split every line into separate entry
    toks = []
    tokes = []
    for i in tok:  # split every entry by spaces
        toks.append(i.split(" "))
    for j in toks:  # filter out comments
        loc = None
        for k in j:
            if k.startswith("#"):
                loc = j.index(k)
            else:
                loc = None
        filterd = j[0:loc]
        tokes.append(filterd)  # generate list of filtered results
    return tokes


def parse(form, inst, args, file):
    f = open(file, 'r')
    fd = f.read()
    f.close()
    l = lex(fd)
    res = []
    err = []
    line_count = 0
    inst_line_count = 0
    for line in l:
        if line[0] in inst:
            inst_line_count += 1
            ls = sub_parse(line, inst, form, args, 0)
            res += ls[0]
            line_count += ls[2]
            if ls[3]:
                err.append([ls[3][0], inst_line_count])
    if len(err) == 0:
        return [res, inst_line_count]
    else:
        return [err, 0]


def sub_parse(line, inst, form, args, c, layer=False):
    if layer is False:
        form_ = form[inst[line[0]][0]]
    elif layer is True:
        form_ = form
    res = []
    err = []
    res_temp = ''
    g = 0
    x = 0
    try:
        for i in form_:
            if type(i) is dict:
                continue
            if layer is False:
                if len(res_temp) == int(inst[line[0]][0].replace(',', ';').split(';')[1:][g]):
                    g += 1
                    res.append(res_temp)
                    res_temp = ''
                    x += 1
            i = i.split(';')
            length = int(i[1])
            if i[0] == '0':
                res_temp += '0' * length
            elif i[0] == '1':
                res_temp += '1' * length
            elif i[0] == 'op':
                res_temp += inst[line[0]][1]
                c += 1
            elif i[0] == 'r':  # TODO: Add check to make sure the register number isn't too large
                l_temp = line[c]
                l_temp = l_temp.split('r')[1]
                if is_imm(l_temp):
                    res_temp += is_imm(l_temp).zfill(length)
                else:
                    err.append('r_err')
                c += 1
                del l_temp
            elif i[0] == 'i':  # TODO: Add check to make sure the imm value is a number and not too large
                if is_imm(line[c]):
                    res_t = is_imm(line[c]).zfill(length)
                    res_temp += res_t
                    del res_t
                else:
                    err.append('i_err')
                c += 1
            elif i[0] == 'var':
                d_temp = form_[-1]
                if d_temp['var']:
                    d_temp = d_temp['var']
                else:
                    err.append('d_err')
                if line[c].startswith('r'):  # TODO: Add check to make sure the register number isn't too large
                    res_t = sub_parse(line, inst, d_temp['r'], args, c, True)
                elif is_imm(line[c]):
                    res_t = sub_parse(line, inst, d_temp['i'], args, c, True)
                elif i[0].startswith('~'):
                    if d_temp[line[c]]:
                        res_t = sub_parse(line, inst, d_temp[line[c]], args, c, True)
                    else:
                        err.append('d_err')
                else:
                    err.append('isnt_err')
                try:
                    err += res_t[3]
                    c = res_t[1]
                    res_t = res_t[0][0]
                    res_temp += res_t
                    del res_t
                    del d_temp
                except NameError:
                    err.append('d_err')
            elif i[0].startswith('~'):
                d_temp = args[i[0].replace('~', '')]
                if d_temp[line[c]]:
                    res_temp += d_temp[line[c]]
                else:
                    err.append('undef_err')
                c += 1
            else:
                err.append('inst_err')
    except IndexError:
        err.append('arg_err')
    if layer is False:
        if c != len(line):
            err.append('arg_err')
    res.append(res_temp)
    del res_temp
    return [res, c, x + 1, err]


def err_handle(tag, x):
    x = str(x)
    if tag == 'i_err':
        return 'Line ' + x + ': Invalid immediate'
    elif tag == 'r_err':
        return 'Line ' + x + ': Invalid register argument(address to large or invalid format)'
    elif tag == 'd_err':
        return 'Line ' + x + ': "var" protocol not defined in IS.yaml'
    elif tag == 'inst_err':
        return 'Line ' + x + ': Format error. Please ensure the IS.yaml format uses correct syntax'
    elif tag == 'undef_err':
        return 'Line ' + x + ': Undefined custom argument. Please define in the IS.yaml file'
    elif tag == 'arg_err':
        return 'Line ' + x + ': Incorrect number of arguments provided'
    else:
        return 'Line ' + x + ": Unknown error. Please report to John 'Jallen' Allen"


def is_imm(val):
    if val.startswith('0x') or val.startswith('0X'):
        if ishex(val[2:]):
            return bin(int(val[2:], 16))[2:]
    elif isbin(val):
        return val
    elif val.isdigit():
        return bin(int(val))[2:]
    else:
        return False


def ishex(numb):
    if all(d in string.hexdigits for d in numb):
        return True


def isbin(numb):  # checks if the number is binary
    for i in numb:
        if i == "0" or i == "1":
            pass
        else:
            return False
    return True


if __name__ == "__main__":
    main()